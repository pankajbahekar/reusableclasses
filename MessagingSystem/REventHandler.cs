﻿using UnityEngine;

/*
 * A simple event system.
 *
 * Usage:
 *
 * class MyActions {
 *     static public REvent SomeAction = new REvent();
 * }
 *
 * class MyButton : MonoBehaviour {
 *     void OnEnable () {
 *         MyActions.SomeAction.OnFire += handleSomeAction;
 *     }
 *     void OnDisable () {
 *         MyActions.SomeAction.OnFire -= handleSomeAction;
 *     }
 *
 *     void OnClick () {
 *         MyActions.SomeAction.Fire("anything");
 *     }
 *
 *     void handleSomeAction (object eventData) {
 *         Debug.Log((string) eventData);
 *     }
 * }
 */


public delegate void REventHandler(object v);
public class REvent {

    event REventHandler _OnFire;
    public event REventHandler OnFire {
        add {
            _OnFire += value;
        }

        remove {
            _OnFire -= value;
        }
    }

    public void Fire (object v) {
        REventHandler temp = _OnFire;
        if (temp != null) {
            if (false) {
                foreach (System.Delegate d in temp.GetInvocationList()) {
                    string message = string.Format("REvent: Calling `{1}` on `{0}`",
                            d.Target,
                            d.Method.Name
                            );
                    Debug.Log(message, temp.Target as UnityEngine.Object);
                }
            }
            temp(v);
        }
    }

    public void Fire () {
        Fire(null);
    }


}
