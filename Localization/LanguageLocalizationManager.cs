﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

/*
 * This class refers streamingAssets/localizedText.json
 * and returns English value or French value after passing
 * the key. You can add LocalizedText.cs class to any text
 * gameObject and give the key. That calss will use
 * LanguageLocalizationManager instance to get appropiate
 * english or french value.
 */
namespace XXXXProjectName.LanguageLocalization
{

    public class LanguageLocalizationManager : MonoBehaviour
    {

        public static LanguageLocalizationManager instance;

        private Dictionary<string, object> localizedText;
        private bool isReady = false;
        private string missingTextString = "Localized text not found";

        void Awake()
        {
            //Check if instance already exists
            if (instance == null)
            {
                //if not, set instance to this
                instance = this;
            }

            //If instance already exists and it's not this:
            else if (instance != this)
            {
                /*
                * Then destroy this. This enforces our singleton pattern,
                * meaning there can only ever be one instance of a LanguageLocalizationManager.
                */
                Destroy(gameObject);
            }

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
            LoadLocalizedText("localizedText.json");
        }

        public void LoadLocalizedText(string fileName)
        {
            localizedText = new Dictionary<string, object>();
            string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

                for (int i = 0; i < loadedData.items.Length; i++)
                {
                    localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
                }

                Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");
            }
            else
            {
                Debug.LogError("Cannot find file!");
            }

            isReady = true;
        }

        public string GetLocalizedValue(string key)
        {
            string result = missingTextString;
            if (localizedText.ContainsKey(key))
            {
                LocalizationString resultObj = localizedText[key] as LocalizationString;

                if (GameManager.instance.selectedLanguage == GameManager.Language.English)
                    result = resultObj.EN;
                else
                    result = resultObj.FR;
            }

            return result;
        }

        public bool GetIsReady()
        {
            return isReady;
        }

    }

    [System.Serializable]
    public class LocalizationData
    {
        public LocalizationItem[] items;
    }

    [System.Serializable]
    public class LocalizationItem
    {
        public string key;
        public LocalizationString value;
    }

    [System.Serializable]
    public class LocalizationString
    {
        public string EN;
        public string FR;
    }
}
