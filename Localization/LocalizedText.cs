﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace XXXXProjectName.LanguageLocalization
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizedText : MonoBehaviour
    {

        public string key;

        private void OnEnable()
        {
            RetrieveTextAndStartDisplaying();
            // Subscribe to some individual message events
            GameManager.GameManagerActions.LanguageChanged.OnFire += OnToggleLanguage;
        }

        private void OnDisable()
        {
            // Unsubscribe to all the individual message events
            GameManager.GameManagerActions.LanguageChanged.OnFire -= OnToggleLanguage;
        }

		    private void OnDestroy()
        {
            // Unsubscribe to all the individual message events
            GameManager.GameManagerActions.LanguageChanged.OnFire -= OnToggleLanguage;
        }

        // Use this for initialization
        void Start()
        {
            RetrieveTextAndStartDisplaying();
        }

        // Get text accroding to the local key value and then render the text
        public void RetrieveTextAndStartDisplaying()
        {
            TMP_Text text = GetComponent<TMP_Text>();
      			if (LanguageLocalizationManager.instance)
      			{
      				text.text = LanguageLocalizationManager.instance.GetLocalizedValue(key);
      			}
        }

        private void OnToggleLanguage(object v)
        {
            // Toggle the language between English and French after the global setting
            // has been changed
            RetrieveTextAndStartDisplaying();
        }

    }
}
