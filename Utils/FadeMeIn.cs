﻿using System.Collections;
using UnityEngine;

namespace XXXXProjectName.Utils
{

    [RequireComponent(typeof(CanvasGroup))]
    public class FadeMeIn : MonoBehaviour
    {

        [SerializeField]
        float fadeInInSeconds = 1f;
        [SerializeField]
        float afterDelayOfSeconds = 1f;
        // Use this for initialization
        void Start()
        {
            LeanTween.alphaCanvas(gameObject.GetComponent<CanvasGroup>(), 1f, fadeInInSeconds).setDelay(afterDelayOfSeconds);
        }
    }
}
