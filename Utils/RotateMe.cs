﻿using System.Collections;
using UnityEngine;

namespace XXXXProjectName.Utils
{
    public class RotateMe : MonoBehaviour
    {

        public bool rotateAlongXAxis;
        public bool rotateAlongYAxis;
        public bool rotateAlongZAxis;

        public float rotationSpeedMutiplier = 1f;
        // Use this for initialization
        void Start()
        {

        }

        void Update()
        {
            if(rotateAlongXAxis)
            {
                transform.Rotate(Time.deltaTime * rotationSpeedMutiplier, 0, 0);
            }
            if (rotateAlongYAxis)
            {
                transform.Rotate(0, Time.deltaTime * rotationSpeedMutiplier, 0);
            }
            if (rotateAlongZAxis)
            {
                transform.Rotate(0, 0, Time.deltaTime * rotationSpeedMutiplier);
            }
        }
    }
}
