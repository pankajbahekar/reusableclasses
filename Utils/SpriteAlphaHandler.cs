﻿using System.Collections;
using UnityEngine;

namespace XXXXProjectName.Utils
{
    // Thing script will change alpha values of children
    public class SpriteAlphaHandler : MonoBehaviour
    {
        public bool shouldLookForSprites;
        public float alphaValue = 1f;
        public Color newColor;

        // Update is called once per frame
        void Update()
        {
			      newColor.a = alphaValue;
            if (shouldLookForSprites)
            {
                foreach (Transform child in transform)
                {
                    if(child.GetComponent<SpriteRenderer>()!=null)
                    child.GetComponent<SpriteRenderer>().color = newColor;
                }
            }
            else
            {
                foreach (Transform child in transform)
                {
                    if (child.GetComponent<Renderer>() != null)
                    {
                        foreach (Material m in child.GetComponent<Renderer>().materials)
                        {
                            m.color = newColor;
                        }
                    }
                }
            }
        }
    }
}
