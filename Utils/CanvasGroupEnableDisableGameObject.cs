﻿using System.Collections;
using UnityEngine;

/*
 * This class enables or disbles interaction with UI elements
 * according to the alpha value.
 * if (alpha == 0) ==> Disable gameObject interaction and if (alpha == 1)
 * vice a versa.
 */
namespace XXXXProjectName.Utils
{
    [RequireComponent(typeof(CanvasGroup))]
    public class CanvasGroupEnableDisableGameObject : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if((float)gameObject.GetComponent<CanvasGroup>().alpha == 1f)
            {
                if(!gameObject.GetComponent<CanvasGroup>().blocksRaycasts)
                {
                    gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
                }
            }
            if ((float)gameObject.GetComponent<CanvasGroup>().alpha == 0f)
            {
                if (gameObject.GetComponent<CanvasGroup>().blocksRaycasts)
                {
                    gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
                }
            }
        }
    }
}
