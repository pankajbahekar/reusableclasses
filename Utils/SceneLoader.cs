﻿using System.Collections;
using UnityEngine;


/* This scene loader loads or unloads the scene
 * just provide if you want to load or unload and
 * select the type ( Single or additive )
 */
namespace XXXXProjectName.Utils
{

    public class SceneLoader : MonoBehaviour
    {
        public enum SceneNames { Scene0, Scene1, Scene2, Scene3};

        private SceneNames SceneNameForStory;

        public enum LoadOrUnload { Load, Unload };
        public LoadOrUnload loadOrUnload;

        private enum LoadSceneAs { Addative,Single };
        private LoadSceneAs selectedLoadSceneType;

        public void OnMouseDown()
        {
            if (loadOrUnload == LoadOrUnload.Load)
            {
                if (Application.CanStreamedLevelBeLoaded(SceneNameForStory.ToString()))
                {
                    if (selectedLoadSceneType == LoadSceneAs.Addative)
                    {
                      // Fire delegate to ask GameManager to load new scene addativily and handle scean transtion
                    }
                    else
                    {
                        // Fire delegate to ask GameManager to load new scene as single and handle scean transtion
                    }
                }
                else
                {
                    //No Scene found
                    Debug.LogError("No scene found while loading.");
                }
            }
            else
            {
                if (Application.CanStreamedLevelBeLoaded(SceneNameForStory.ToString()))
                {
                  // Fire delegate to ask GameManager to unload current scene and handle scean transtion
                }
                else
                {
                    //No Scene found
                    Debug.LogError("No scene found while unloading.");
                }
            }
        }
    }
}
