﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace XXXXProjectName.Utils
{
  public class LookAtTarget : MonoBehaviour
  {
      /*
       * Add this script to any object and drag target to look at. Check the axis on which object is allowed to rotate
       */
      public Transform Target;

      // Set if this object can rotate around given axis
      public bool canRotateInXAxis = false;
      public bool canRotateInYAxis = false;
      public bool canRotateInZAxis = false;

      // Temp position holders
      float xPosition;
      float yPosition;
      float zPosition;

      // Disable this script if boll is checked after 1 frame update
      public bool DisableMeAfterFrameUpdate = false;

      void Start(){
          // Set by default target as camera
          if (GameObject.FindWithTag("MainCamera") != null)
              Target = GameObject.FindWithTag("MainCamera").transform;
      }
      // Update is called once per frame
      void Update()
      {
          // Make sure the Target reference is set
          Assert.IsNotNull(Target);

          if (canRotateInXAxis)
              xPosition = this.transform.position.x;
          else
              xPosition = Target.position.x;

          if (canRotateInYAxis)
              yPosition = this.transform.position.y;
          else
              yPosition = Target.position.y;

          if (canRotateInZAxis)
              zPosition = this.transform.position.z;
          else
              zPosition = Target.position.z;


          Vector3 targetPostition = new Vector3(xPosition,
                                                yPosition,
                                                zPosition);
          this.transform.LookAt(targetPostition);

          if (DisableMeAfterFrameUpdate)
              gameObject.GetComponent<LookAtTarget>().enabled = false;
      }
  }
}
