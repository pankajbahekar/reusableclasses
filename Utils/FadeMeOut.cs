﻿using System.Collections;
using UnityEngine;

namespace XXXXProjectName.Utils
{

    [RequireComponent(typeof(CanvasGroup))]
    public class FadeMeOut : MonoBehaviour
    {

        [SerializeField]
        float fadeOutInSeconds = 1f;
        [SerializeField]
        float afterDelayOfSeconds = 1f;
        // Use this for initialization
        void Start()
        {
            LeanTween.alphaCanvas(gameObject.GetComponent<CanvasGroup>(), 0f, fadeOutInSeconds).setDelay(afterDelayOfSeconds);
        }
    }
}
