﻿using System.Collections;
using UnityEngine;

namespace XXXXProjectName.Utils
{

  public class PingPongJump : MonoBehaviour {

      public float moveTo;
      public float moveTime;

  	// Use this for initialization
  	void Start () {
          LeanTween.moveY(gameObject, moveTo, moveTime).setEase(LeanTweenType.easeInQuad).setLoopPingPong();
  	}
  }
}
